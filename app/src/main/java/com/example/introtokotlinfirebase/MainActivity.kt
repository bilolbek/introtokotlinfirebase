package com.example.introtokotlinfirebase

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener





class MainActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth?= null
    private var currentUser: FirebaseUser?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val database = FirebaseDatabase.getInstance()
        val databaseRef = database.getReference("message")

        mAuth = FirebaseAuth.getInstance()

        mAuth!!.signInWithEmailAndPassword("paulo@me.com","password")
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Signed In Successful", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Signed In Unsuccessful", Toast.LENGTH_LONG).show()
                }
            }
        var employee = Employee(
            "Bilolbek Atabekov", "Senior developer?",
            "E 652 St Apt 10 10", 21, "Volleybal"
        )
        //databaseRef.setValue(employee)

        databaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val value = snapshot.value as HashMap<String, Any>

                //Log.d("VALUE: ", value!!.get("name").toString())
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("ERROR: ", error.message)
            }

        })
    }

    override fun onStart() {
        super.onStart()
        currentUser = mAuth!!.currentUser
        if(currentUser != null){
            Toast.makeText(this, "User is logged in", Toast.LENGTH_LONG).show()
        }
        Toast.makeText(this, "User is logged out", Toast.LENGTH_LONG).show()

    }

    data class Employee(
        var name: String,
        var position: String,
        var homeAddress: String,
        var age: Int,
        var hobby: String
    )
}